import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

public class FlashCardPanel extends JLayeredPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String front = "front";
	private String back = "back";
	private JLabel text = new JLabel(front);
	private Color frontcolour = new Color(204, 255, 229);
	private Color backcolour = new Color(229, 255, 204);

	FlashCardPanel() {
		this.setPreferredSize(new Dimension(this.getWidth(), 150));
		this.setLayout(new FlowLayout());
		text.setOpaque(true);
		text.setVerticalAlignment(JLabel.TOP);
		text.setHorizontalAlignment(JLabel.CENTER);
		text.setBackground(new Color(229, 255, 204));
		text.setBorder(BorderFactory.createEtchedBorder(Color.black, Color.GRAY));
		text.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		text.setForeground(Color.BLACK);
		add(text);
		text.setPreferredSize(new Dimension(250, 150));
		text.setBounds(0, 0, 250, 150);
	}

	public void setFront(String f) {
		front = f;
		text.setText(front);
		text.setBackground(frontcolour);
	}

	public void setBack(String b) {
		back = b;
		text.setText(back);
		text.setBackground(backcolour);
	}

	public String getFront() {
		return front;
	}

	public String getBack() {
		return back;
	}

	public void switchcard() {
		if (text.getText().equals(front)) {
			text.setBackground(backcolour);
			text.setText(back);
		} else if (text.getText().equals(back)) {
			text.setBackground(frontcolour);
			text.setText(front);
		}
	}

	public void reset() {
		front = "front";
		back = "back";
		text.setText(front);
	}
}
