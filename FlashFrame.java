
/**
 *  2016/04/15: Fix card colour mixup
 *  2016/04/15: Fix problem causing index out of range error
 *  2016/04/15: Fix problem that causes next card to be shown in stead of "front" at end of test
 *  2016/04/15: Add Help menu item and About dialog box.
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;
import java.util.Scanner;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * @author Jannetta S Steyn
 *
 */
public class FlashFrame extends JFrame implements ActionListener, ItemListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	final JFileChooser fc = new JFileChooser();

	// declare items
	JMenuBar mb = new JMenuBar();
	JMenu file_menu = new JMenu("File");
	JMenu test_menu = new JMenu("Cards");
	JMenu help_menu = new JMenu("Help");
	JMenuItem file_open = new JMenuItem("Open");
	JMenuItem file_new = new JMenuItem("New");
	JCheckBoxMenuItem test_mixed = new JCheckBoxMenuItem("Mixed up");
	JRadioButtonMenuItem test_all = new JRadioButtonMenuItem("All");
	JRadioButtonMenuItem test_five = new JRadioButtonMenuItem("Five");
	JRadioButtonMenuItem test_ten = new JRadioButtonMenuItem("Ten");
	JMenuItem help_about = new JMenuItem("About");
	JMenuItem help_help = new JMenuItem("Help");
	JLabel label = new JLabel("Filename: ");
	JLabel filename = new JLabel();
	JPanel one = new JPanel();
	JPanel p_left = new JPanel();
	FlashCardPanel p_card = new FlashCardPanel();
	Panel_Message p_right = new Panel_Message();
	JTextField t_answer = new JTextField(30);
	JPanel p_answer = new JPanel();
	JPanel p_button = new JPanel();
	JLabel correct = new JLabel();
	JButton next = new JButton("Next");
	JButton show = new JButton("Show");
	String side = "front";
	int cardshown = -1;
	int cardnumb;
	int cardstart = 0;

	int[] count;
	boolean mixup = true;
	boolean first = true;
	int five = 0;

	Hashtable<String, String> cards;
	ArrayList<String> keys = new ArrayList<String>();
	ArrayList<Integer> mixedup = new ArrayList<Integer>();
	ArrayList<Integer> score;

	Random random = new Random();

	FlashFrame(String string) {
		super(string);
		setSize(650, 480);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		test_mixed.setSelected(mixup);

		// Add menu
		mb.add(file_menu);
		mb.add(test_menu);
		mb.add(help_menu);

		// Add menu items;
		file_menu.add(file_open);
		file_menu.add(file_new);
		test_menu.add(test_mixed);
		test_menu.addSeparator();
		ButtonGroup testgroups = new ButtonGroup();
		test_menu.add(test_all);
		test_all.setSelected(true);
		test_menu.add(test_five);
		test_menu.add(test_ten);
		testgroups.add(test_all);
		testgroups.add(test_five);
		testgroups.add(test_ten);
		help_menu.add(help_about);
		help_menu.add(help_help);

		// Add actionlisteners
		file_open.addActionListener(this);
		file_new.addActionListener(this);
		test_mixed.addItemListener(this);
		test_all.addActionListener(this);
		test_five.addActionListener(this);
		test_ten.addActionListener(this);
		help_about.addActionListener(this);
		help_help.addActionListener(this);
		t_answer.addActionListener(this);
		next.addActionListener(this);
		show.addActionListener(this);

		// Set up frame
		setJMenuBar(mb);

		one.add(label);
		one.add(filename);
		t_answer.setText("Type your answer in here.");
		t_answer.selectAll();
		p_answer.add(t_answer);
		p_answer.add(correct);
		next.setEnabled(false);
		p_button.add(next);
		p_button.add(show);
		getContentPane().add(one);
		p_card.setAlignmentX(CENTER_ALIGNMENT);
		getContentPane().add(p_left);
		getContentPane().add(p_card);
		getContentPane().add(p_right);
		getContentPane().add(p_answer);
		getContentPane().add(p_button);
		openDatabase();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Open")) {
			openDatabase();
			reset();
			zeroscore();
		} else if (e.getActionCommand().equals("New")) {
			System.out.println("New");
		} else if (e.getActionCommand().equals("Next")) {
			nextcard();
		} else if (e.getActionCommand().equals("Show")) {
			p_card.switchcard();
		} else if (e.getActionCommand().equals("Five")) {
			five = 5;
			reset();
			zeroscore();
			randomize();
		} else if (e.getActionCommand().equals("Ten")) {
			five = 10;
			reset();
			zeroscore();
			randomize();
		} else if (e.getActionCommand().equals("All")) {
			five = 0;
			reset();
			zeroscore();
			randomize();
		} else if (e.getActionCommand().equals("About")) {
			// default title and icon
			JOptionPane.showMessageDialog(this,
					"FlashCards is developed by Jannetta Steyn.\n" + "Copyright (c): Jannetta S Steyn\n"
							+ "FlashCards should run on any operating system that runs Java.\n"
							+ "Please use the issue tracker at https://bitbucket.org/jsteyn/flashcards\n"
							+ "if you experience any problems with the program.");
			// + "FlashCards is open source and should run on any operating
			// system that runs Java.\n"
			// + "FlashCards is licensed under the terms of the GNU General
			// Public Licence\n"
			// + "Version 3 as public by the Free Software Foundation.");
		} else if (e.getActionCommand().equals("Help")) {
			JOptionPane.showMessageDialog(this, "1. Press the Next button to get the next card.\n"
					+ "2. Press the Show button to show the other side of the card.\n"
					+ "3. Press File and then Open to open a new card file.\n" + "\n"
					+ "Card files are plain comma separated value (CSV) text files with a .cards extention.\n"
					+ "Each line in the file contains the information for one card. The content for the front\n"
					+ "and back of each card is separated by a comma, e.g.\n" + "front,back\n" + "english,german\n");
		} else {
			// if
			// (cards.get(keys.get(mixedup.get(cardshown))).equals(t_answer.getText()))
			// {
			// cards.get(keys.get(mixedup.get(cardshown)));
			// JOptionPane.showMessageDialog(this, "Yip");
			// } else {
			// JOptionPane.showMessageDialog(this, "Nope");
			// System.out.println(cards.get(keys.get(mixedup.get(cardshown))));
			// }
			System.out.println(e.getActionCommand());
		}

	}

	/**
	 * Make sure the mixedup array is initialised. It should be the length of
	 * the number of cards and by default have elements in the same order as
	 * read in from the database
	 */
	private void init_mixedup() {
		for (int i = 0; i < cards.size(); i++) {
			mixedup.set(i, i);
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		int state = e.getStateChange();
		if (state == ItemEvent.SELECTED) {
			mixup = true;
		} else {
			mixup = false;
		}

	}

	/**
	 * @param file
	 *            File to read Read records from file. Each line contains two
	 *            words, separated by a comma. The first word is the front of
	 *            the card and the second word is the back
	 */
	private void readCards(File file) {
		try {
			// Read selected file and add cards.
			// File must be formatted as one line per card and comma separated
			// First value is the front of the card, second value is the back of
			// the card
			Scanner sc = new Scanner(file);
			while (sc.hasNextLine()) {
				String[] tokens = new String[2];
				String line = sc.nextLine();
				tokens = line.split(",");
				cards.put(tokens[0], tokens[1]);
				keys.add(tokens[0]);
			}
			zeroscore();
			if (mixup) {
				randomize();
			} else {
				init_mixedup();
			}
			sc.close();
			cardnumb = cards.size();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

	}

	/**
	 * Zero the score keeping variables
	 */
	public void zeroscore() {
		score = new ArrayList<Integer>();
		for (int i = 0; i < cards.size(); i++) {
			score.add(i, -1);
		}
	}

	/**
	 * Mix up the elements in array mixup to change the order of cards from the
	 * default.
	 */
	private void randomize() {
		Random random = new Random();
		mixedup = new ArrayList<Integer>();
		int j = random.nextInt(cards.size());
		// initialise
		for (int i = 0; i < cards.size(); i++) {
			mixedup.add(-1);
		}
		// randomize
		for (int i = 0; i < cards.size(); i++) {
			while (mixedup.contains(j)) {
				j = random.nextInt(cards.size());
			}
			mixedup.add(i, j);
		}
		first = true;
		if (five > 0) {
			cardstart = new Random().nextInt(cards.size());
			if (cardstart > cards.size() - five) {
				cardstart = cards.size() - five;
			}
			cardshown = cardstart - 1; // minus 1 because their is an increment
										// at the beginning of the "next"
			cardnumb = cardstart + five;
		} else {
			cardstart = 0;
			cardnumb = cards.size();
			cardshown = -1;
		}
	}

	private void openDatabase() {
		cards = new Hashtable<String, String>();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("FlashCard file (*.cards)", "cards");
		fc.addChoosableFileFilter(filter);
		fc.setAcceptAllFileFilterUsed(false);
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = fc.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			filename.setText(file.getName());
			readCards(file);
			if (mixup) {
				randomize();
			} else {
				init_mixedup();
			}
			next.setEnabled(true);
		} else {
			System.out.println("File open cancelled");
		}

	}

	/**
	 * Reset to pre-test conditions
	 */
	public void reset() {
		p_card.reset();
		t_answer.setText("Type your answer in here.");
		t_answer.selectAll();
		p_right.setText("");
		zeroscore();
	}

	public void nextcard() {
		if (t_answer.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Type an answer in the text box.");
		} else {
			if (!first) {
				// Mark answer
				if (t_answer.getText().equals(cards.get(keys.get(mixedup.get(cardshown))))) {
					score.set(cardshown, 1);
					JOptionPane.showMessageDialog(this, "Yip");
				} else {
					JOptionPane.showMessageDialog(this,
							"Nope, the answer is " + cards.get(keys.get(mixedup.get(cardshown))));
					score.set(cardshown, 0);
				}
			}
			first = false;
			// Initialize textarea for next answer
			t_answer.setText("");
			cardshown++;
			// If at the end at the number of cards to be included in test
			if (cardshown == cardnumb) {
				int right = 0;
				@SuppressWarnings("unused")
				int wrong = 0;
				cardshown = cardstart - 1;
				// Add up correct and wrong answers
				for (int i = cardstart; i < score.size(); i++) {
					if (score.get(i) == 0)
						wrong++;
					if (score.get(i) == 1) {
						right++;
					}
				}
				// Show score
				JOptionPane.showMessageDialog(this, "You scored " + right + " out of " + (cardnumb - cardstart));
				// If mix up is ticked, mix cards up for next test
				reset();
				if (mixup) {
					randomize();
				} else {
					init_mixedup();
				}
			} else {
				p_right.setText("Card " + (cardshown - cardstart + 1) + " of " + (cardnumb - cardstart));
				// Key for next card (i.e. front of card)
				if (cardshown > -1) {
					String key = keys.get(mixedup.get(cardshown));
					p_card.setFront(cards.get(key));
					p_card.setBack(key);
				}
			}
		}

	}

}
