import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Panel_Message extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JTextArea text = new JTextArea();
	
	Panel_Message() {
		super();
		add(text);
	}
	
	public void setText(String txt) {
		text.setText(txt);
	}
	
	
}
