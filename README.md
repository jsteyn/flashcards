# README #

I wrote FlashCards for my son to learn his German vocabulary. I wanted something simple and that is what this program is - simple but functional.

There are a couple of vocabulary files that can be downloaded as examples. These are plain text files and can be created with any simple text editor.

If you like the program please let me know. If there is a bug please submit an issue and I'll try my best to sort it out.